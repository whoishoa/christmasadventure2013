<?php include 'library.php' ?>

<?php

/**
 * File Name: index.php
 * Description: This file distributes the 
 * @author Hoa Mai
 * @copyright 2013
 */
 // phpinfo();
 session_start();

 connect();

 // addTableItem('Hoa', 5, 5, 'Hoa.png', 0, '150');
 
 // echo hasItemName('Hoa');
 
?>

<!DOCTYPE HTML>
<html>
 <head>
  <title>A Christmas Adventure</title>
  <!-- Scripts goes here -->
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script src="http://www.textbookfriends.com/select2/select2.js"></script>
  <script>
  
  window.onload = start;
  
  // setInterval(updateMap(),100);
  
  $(function(){
    $(window).resize(function() {
      refresh();
    });

    // JQuery Methods area
  
  });
  
  $(document).keydown(function(e){
    if (Content.currPage == 'console') {
        if (e.keyCode == 37) { 
            move('left');
        } else if (e.keyCode == 38) {
            move('up');
        } else if (e.keyCode == 39) {
            move('right');
        } else if (e.keyCode == 40) {
            move('down');
        } else if (e.keyCode == 32) {
            spacePressed();
        }
    }
  });
    
  var Content = function() { }; 
  Content.min_height = 520;
  Content.min_width = 560;
  Content.side_offset = 50;
  Content.tileSize = 40;
  Content.borderSize = 2;
  Content.overestimate = 20;
  Content.gridWidth = 11;
  Content.gridHeight = 11;
  Content.hasStarted = false;
  Content.pages = ['intro', 'console', 'help'];
  Content.currPage = 'intro';
  Content.currMaze = null;
  
  // User's attributes
  Content.user = '';
  Content.currX = 5;
  Content.currY = 5;
  Content.moveSpeed = 150; // time in milliseconds to move between squares
  Content.moving = false;
  
  
  function refresh() {
    if ( $( window ).width() - 2 * Content.side_offset - Content.overestimate > Content.min_width ) {
        $('#content').width( $( window ).width() - 2 * Content.side_offset - Content.overestimate);
    } else {
        $('#content').width( Content.min_width );
    }
    
    if (Content.hasStarted) {
        if ( $( window ).height() - 2 * Content.side_offset - Content.overestimate > Content.min_height ) {
            $('#content').height( $( window ).height() - 2 * Content.side_offset - Content.overestimate);
        } else {
            $('#content').height( Content.min_height );
        }
    } else {
        $('#content').height( $('#startButton').position().top );
    }
    
    // Set the position of the map
    $('#map').css('top', ($( window ).height() - Content.overestimate - $('#map').height()) / 2 );
    $('#map').css('left', ($( window ).width() - Content.overestimate - $('#map').width()) / 2 );
    
    // Adjusting the layer that covers the map
    $('#coverTop').css('height', ($('#map').position().top + Content.borderSize + Content.tileSize) + 'px');
    $('#coverTop').css('width', $( window ).width() + 'px');
    
    $('#coverBot').css('height', ($('#map').position().top + Content.borderSize + Content.tileSize + Content.overestimate) + 'px');
    $('#coverBot').css('width', $( window ).width() + 'px');
    
    $('#coverLeft').css('height', $( window ).height() + 'px');
    $('#coverLeft').css('width', ($('#map').position().left + Content.borderSize + Content.tileSize + 1) + 'px');
    
    $('#coverRight').css('height', $( window ).height() + 'px');
    $('#coverRight').css('width', ($('#map').position().left + Content.borderSize + Content.tileSize + Content.overestimate) + 'px');
    
    // Adjust the position of the arrows
    $('#directionUp').css('top', ($('#map').position().top + Content.borderSize + Content.tileSize + Content.tileSize / 2) + 'px');
    $('#directionUp').css('left', ($('#map').position().left + ($('#map').width() - $('#directionUp').width()) / 2) + 'px');
    
    $('#directionDown').css('bottom', ($('#map').position().top + Content.borderSize + Content.tileSize + (Content.tileSize + Content.overestimate) / 2) + 'px');
    $('#directionDown').css('left', ($('#map').position().left + ($('#map').width() - $('#directionDown').width()) / 2) + 'px');
    
    $('#directionLeft').css('top', $('#map').position().top + ($('#map').height() - $('#directionLeft').height()) / 2 + 'px');
    $('#directionLeft').css('left', ($('#map').position().left + Content.borderSize + Content.tileSize + Content.tileSize / 2) + 'px');
    
    $('#directionRight').css('top', $('#map').position().top + ($('#map').height() - $('#directionLeft').height()) / 2 + 'px');
    $('#directionRight').css('right', ($('#map').position().left + Content.borderSize + Content.tileSize + (Content.tileSize + Content.overestimate) / 2 ) + 'px');
    
    // Reposition the player icon
    var centerId = '#r' + Math.floor(Content.gridHeight / 2) + 'c' + Math.floor(Content.gridWidth / 2);
    $('#playerIcon').css('top', $('#map').position().top + $(centerId).position().top);
    $('#playerIcon').css('left', $('#map').position().left + $(centerId).position().left);
  }
  
  function start() {
    $('#nameSelect').select2({
        placeholder: 'Select your name'
    });
    showPage('intro');
  }
  
  function startClicked() {
    if ($('#nameSelect').val() == '') {
        alert('You must select who you are before you can start');
    } else {
        Content.user = $('#nameSelect').val();
        Content.hasStarted = true;
        showPage('console');
        var centerId = '#r' + Math.floor(Content.gridHeight / 2) + 'c' + Math.floor(Content.gridWidth / 2);
        $('#player').attr('src', Content.user + '.png');
        $('#playerIcon').css('top', $('#map').position().top + $(centerId).position().top);
        $('#playerIcon').css('left', $('#map').position().left + $(centerId).position().left);
        $.get( "maze2.php", mazeLoaded, "json" );
    }
  }
  
  function mazeLoaded( data ) {
    Content.currMaze = data;
    // TODO: get and set the user's location
    $.get( "entermaze.php", { 'user' : Content.user }, initPosLoaded, "json" );
  }
  
  function initPosLoaded( data ) {
    Content.currX = data.col;
    Content.currY = data.row;
    setPosition(Content.currX, Content.currY);
  }
  
  
  
  function setPosition(x, y) {
    Content.currX = x;
    Content.currY = y;
    
    // $.get("updatepos.php", { 'x' : newX, 'y' : newY });
    
    $('#map').css('top', ($( window ).height() - Content.overestimate - $('#map').height()) / 2 );
    $('#map').css('left', ($( window ).width() - Content.overestimate - $('#map').width()) / 2 );
    for (var r = 0; r < Content.gridHeight; r++) {
        for (var c = 0; c < Content.gridWidth; c++) {
            var tileId = 'r' + r + 'c' + c;
//            document.getElementById(tileId).setAttribute('class', 'mazeCell pathCell imageCell');

            if (Content.currMaze[y + r - Math.floor(Content.gridHeight / 2)][x + c - Math.floor(Content.gridWidth / 2)] == 'X') {
                document.getElementById(tileId).setAttribute('class', 'mazeCell treeCell imageCell');
            } else {
                document.getElementById(tileId).setAttribute('class', 'mazeCell pathCell imageCell');
            }

        }
    }
  }
  
  /*
  function updateMap() {
    if (Content.hasStarted) {
        $.get("getsurrounding", )
    }
  }
  */
  
  function showPage(page) {
    for (var i = 0; i < Content.pages.length; i++) {
        $('#' + Content.pages[i]).hide();
    }
    $('#' + page).show();
    Content.currPage = page;
    refresh();
  }
  
  function move(direction) {
    if (Content.moving) {
        return;
    }
    var newX = Content.currX;
    var newY = Content.currY;
    var newLeft = '+=0';
    var newTop = '+=0';
    if (direction == 'left') {
        newX -= 1;
        newLeft = '+=' + Content.tileSize;
    } else if (direction == 'right') {
        newX += 1;
        newLeft = '-=' + Content.tileSize;
    } else if (direction == 'up') {
        newY -= 1;
        newTop = '+=' + Content.tileSize;
    } else if (direction == 'down') {
        newY += 1;
        newTop = '-=' + Content.tileSize;
    } else {
        alert('Hoa is a fail programmer. There is a bug!');
        return;
    }
    
    if (Content.currMaze[newY][newX] == 'X') {
        
    } else {
        
        Content.moving = true;
        $( "#map" ).animate({
            left: newLeft,
            top: newTop
        }, Content.moveSpeed, 'linear', function() {
            // Animation complete.
            Content.moving = false;
            setPosition(newX, newY);
        });
        
    }
    
  }
  
  function spacePressed() {
    // Content.moveSpeed = 450;
  }
  
  </script>
  
  <!-- Style rules goes here -->
  <link rel='stylesheet' href='common.css' />
  <link href="http://www.textbookfriends.com/select2/select2.css" rel="stylesheet"/>
  <style>
  
  body {
    font-family:verdana;
    font-size:small;
  }
  
  #content {
    left:50px;
    top:50px;
    padding:10px;
    width:1000px;
    height:500px;
  }
  
  #startButton {
    background-color:#33B5DB;
    color:white;
    height:40px;
  }
  
  #helpButton {
    top:10px;
    right:10px;
    background-color:red;
    color:white;
    height:30px;
    width:80px;
  }
  
  #returnButton {
    background-color:#33B5DB;
    color:white;
    height:50px;
    width:250px;
    font-size:medium;
    font-weight:900;
  }
  
  .mazeCell {
    height: 40px;
    width: 40px;
  }
  
  .imageCell {
    background-size:100% 100%;
  }
  
  .treeCell {
    background-image:url('http://images.neopets.com/nq2/t/fors.gif');
  }
  
  .pathCell {
    background-image:url('http://images.neopets.com/nq2/t/snow.gif');
  }
  
  </style>
  
 </head>
 <body>
 
  <div class='backgroundImage'>
   <img src='http://www.savemefromboredom.com/images/christmas-wallpaper1/christmas-wallpaper-3.jpg' 
     width='100%' height='100%' alt='' />
  </div>
  
  <div id='content' class='whiteBackground modal curve10 padding10'>
   <div class='center'>
    <img src='title.png' />
   </div>
   
   <div id='intro'>
       <p>
        Hello my siblings, this is your brother Hoa speaking. For too long, I have watched you solve easy puzzles 
        to obtain your presents Christmas after Christmas.
       </p>
       <p>
        Your brother Anh has always prevented me from devising the most clever puzzles that truly tests your intellectual
        capabilities. This year, I have finally decided to break away from his plotless regime and I have gotten 
        presents for you all myself. But before you get too excited, in order to get your Christmas present from me 
        this year, you must find it in my virtual maze. There are several different presents scattered throughout the 
        maze. If you find a present you like, you may pick it up. But beware, you can only pick up one present and you 
        cannot drop presents that you have already picked up. You will know that you have found a present when you see 
        the following icon:
       </p>
       <div class='center' >
        <img src='presents.png' style='border:1px solid black;'/>
       </div>
       <p>
        Please note that the maze is very big. Use the arrow keys to move around the maze. You can also use the 
        space button to add a breadcrumb the space that you are standing on. If there is already a breadcrumb on 
        the space that you are standing on, the space button will pick up that breadcrumb. As space with a 
        breadcrumb looks as follows:
       </p>
       <div class='center' >
        <img src='breadcrumb.png' style='border:1px solid black;'/>
       </div>
       <p>
        Be careful because 
        everyone can see and pick up your breadcrumbs and you can see and pick up their's. You can't add a 
        breadcrumb when you are standing on a present. Use the space button to peek inside the present and you 
        will be prompted about whether you want to pick up the present or not.
       </p>
       <p>
        So what are you waiting for? Please select your name and click 'Start' to begin exploring the maze and find 
        your dream present before somebody else takes it.
       </p>
       
       <table id='startButton' class='center curve5 padding10'>
        <tr>
         <td>
          <select id='nameSelect' style='width:250px;'>
           <option></option>
           <option value='Vu'>Cocky Vu</option>
           <option value='BN'>Bich Ngoc</option>
           <option value='Anh'>Anh the Radioactive Man</option>
           <option value='Thu'>Conservative Thu</option>
           <option value='Hoa'>Hoa's Evil Twin</option>
           <option value='Nga'>Doctor Nga</option>
           <option value='Minh'>Minh Da Bomb</option>
           <option value='Vincente'>Vincente the Pooper</option>
           <option value='Sally'>Bunny Sally</option>
          </select>
         </td>
         <td class='clickable' style='width:100px;' onclick='startClicked()'>
          START
         </td>
        </tr>
       </table>
       
   </div>
   <div id='console'>
       <table id='map' class='condense fixed' style='border:2px solid black;top:0px;left:0px;'>
       
       <?php
        for ($r = 0; $r < 11; $r++) {
            echo "<tr>";
            for ($c = 0; $c < 11; $c++) {
                if (rand(0,5) == 4) {
                    echo "<td id='r" . $r . "c" . $c . "' class='mazeCell treeCell imageCell'>";
                } else {
                    echo "<td id='r" . $r . "c" . $c . "' class='mazeCell pathCell imageCell'>";
                }
                
                echo "</td>";
            }
            echo "</tr>";
        }
       ?>
       
       </table>
       <div id='playerIcon' class='fixed clickable' style='top:0px;left:0px' onclick='spacePressed()'>
         <img id='player' src='Hoa.png' />
       </div>
       
       <div id='coverTop' class='fixed whiteBackground' style='top:0px;left:0px;'></div>
       
       <div id='coverBot' class='fixed whiteBackground' style='bottom:0px;left:0px;'></div>
       
       <div id='coverRight' class='fixed whiteBackground' style='top:0px;right:0px;'></div>
       
       <div id='coverLeft' class='fixed whiteBackground' style='top:0px;left:0px;'></div>
       
       <div id='directionUp' class='fixed clickable' style='opacity:.6' onclick="move('up');">
         <img src='directionUp.png' />
       </div>
       
       <div id='directionLeft' class='fixed clickable' style='opacity:.6' onclick="move('left');">
         <img src='directionLeft.png' />
       </div>
       
       <div id='directionRight' class='fixed clickable' style='opacity:.6' onclick="move('right');">
         <img src='directionRight.png' />
       </div>
       
       <div id='directionDown' class='fixed clickable' style='opacity:.6' onclick="move('down');">
         <img src='directionDown.png' />
       </div>
       
       <table id='helpButton' class='modal curve5 clickable' onclick="showPage('help');">
        <tr>
        <td class='center'>
        HELP
        </td>
        </tr>
       </table>
   </div>
   
   <div id='help'>
   
    <table id='returnButton' class='curve5 clickable center' onclick="showPage('console');">
        <tr>
        <td class='center'>
        Back to the Maze
        </td>
        </tr>
    </table>
   
   </div>
   
  </div>
  
 </body>
</html>
