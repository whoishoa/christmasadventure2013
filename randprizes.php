<?php include 'library.php' ?>

<?php

/**
 * @author 
 * @copyright 2014
 */

 session_start();

 connect();

 $movementSpeed = '150';
 
 // row and column rules:
 /*
  * Must be a odd number greater than 5 and less than 105
  */
 

 $_SESSION['user'] = $_GET['user'];
 
 for ($i = 1; $i <= 9; $i++ ) {
     if (! hasItemName( "prize$i" )) {
        if ($i <= 7) {
            addTableItem("prize$i", 
                    5 + rand(0, 50) * 2, 
                    5 + rand(33 + 16 * ($i - 1), 33 + 16 * ($i)) * 2,
                    "prize$i.png", 0, '0');
        } else {
            addTableItem("prize$i", 
                    5 + rand(0, 50) * 2, 
                    5 + rand(150 + 25 * ($i - 8), 150 + 25 * ($i - 7)) * 2, 
                    "prize$i.png", 0, '0');
        }
     } else {
        if ($i <= 7) {
            setItemInfo("prize$i",'col',5 + rand(33 + 16 * ($i - 1), 33 + 16 * ($i)) * 2);
        } else {
            setItemInfo("prize$i",'col',5 + rand(150 + 25 * ($i - 8), 150 + 25 * ($i - 7)) * 2);
        }
        setItemInfo("prize$i",'row',5 + rand(0, 50) * 2);
     }
 }
?>